const db = require("../db/models");

export async function getPaymentMethod(user_id: string) {
  try {
    const { dataValues } = await db.card.findOne({
      where: { user_id: user_id },
    });
    return dataValues;
  } catch (err) {
  }
}

export async function createPayment(card: object) {
  try {
    const { dataValues } = await db.card.create({ ...card });
    return dataValues;
  } catch (err) {
    throw new Error("Payment card not created");
  }
}

export async function getCardById(card_id: string) {
  try {
    const { dataValues } = await db.card.findOne({ where: { id: card_id } });
    return dataValues;
  } catch (err) {
    throw new Error("This card not found!");
  }
}

export async function deletePayment(cardExtId: string, user_id: string) {
  try {
    const { dataValues } = await db.card.destroy({
      where: {
        external_id: cardExtId,
        user_id: user_id,
      },
    });
    return dataValues;
  } catch (err) {
    throw new Error("Payment was not deleted!");
  }
}

export async function generateTransaction(data: object) {
  try {
    const { dataValues } = await db.transaction.create({ ...data });
    return dataValues;
  } catch (err) {
    throw new Error("Transaction was not created");
  }
}

export async function updateTransaction(order_id: string, order: object) {
  try {
    const { dataValues } = await db.transaction.update(
      { ...order },
      { where: { order_id: order_id } }
    );
    return dataValues;
  } catch (err) {
    throw new Error("Transaction was not updated!");
  }
}

export async function getChargeForRefund(order_id: string, card_id: string) {
  try {
    const { dataValues } = await db.transaction.findOne({
      where: {
        order_id: order_id,
        card_id: card_id,
      },
    });
    return dataValues;
  } catch (err) {
    throw new Error("Charge was not found!");
  }
}
