import { Router } from "express";
import {
  handleCreatingPaymentMethod,
  handleDeletingPaymentMethod,
  handlePayForOrder,
  handleRefundForOrder,
} from "./controllers";
import {verifyRole} from "../middlewares/verifyRole";

const paymentRoutes = Router();

paymentRoutes.post("/card/create", handleCreatingPaymentMethod);
paymentRoutes.delete("/card/delete", handleDeletingPaymentMethod);
paymentRoutes.post("/card/pay", handlePayForOrder);

paymentRoutes.use(verifyRole(1));
paymentRoutes.post("/card/refund/:id", handleRefundForOrder);

export default paymentRoutes;
