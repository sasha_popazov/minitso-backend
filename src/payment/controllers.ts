const stripe = require("stripe")(process.env.SECRET_KEY);
import express from "express";
import { getUserById, updateUser } from "../user/services";
import {
  createPayment,
  deletePayment,
  generateTransaction,
  getCardById,
  getChargeForRefund,
  getPaymentMethod,
  updateTransaction,
} from "./services";
import { getUserFromCookies } from "../middlewares/verify";
import {
  checkingExistOrder,
  checkingPaidOrder, deleteOrder,
  updateStatusOrder,
} from "../order/services";

export async function handleCreatingPaymentMethod(
  req: express.Request,
  res: express.Response
) {
  const user = getUserFromCookies(req, res);
  // @ts-ignore
  const userId = user.subject.id;

  const bodyOrder = req.body;

  let users = await getUserById(userId);

  if (users && !users.customer_id) {
    const customer = await stripe.customers.create({
      email: users.email,
      name: `${users.first_name} ${users.last_name}`,
    });
    users.customer_id = customer.id;

    await updateUser(userId, { ...users });
  }

  let cards = await getPaymentMethod(userId);

  if (!cards) {
    const token = await stripe.tokens.create({
      card: {
        number: bodyOrder.number,
        exp_month: bodyOrder.exp_month,
        exp_year: bodyOrder.exp_year,
        cvc: bodyOrder.cvc,
      },
    });

    const source = await stripe.customers.createSource(users.customer_id, {
      source: token.id,
    });

    const newCard = await createPayment({
      brand: "MasterCard",
      external_id: source.id,
      user_id: users.id,
    });

    res.json(newCard);
    res.end();
  }
}

export async function handleDeletingPaymentMethod(
  req: express.Request,
  res: express.Response
) {
  const user = getUserFromCookies(req, res);
  // @ts-ignore
  const userId = user.subject.id;

  const card = req.body;

  const findCard = await getCardById(card.id);
  if (!findCard) {
    res.status(404).json("Card not found!");
  }

  const cardExtId = findCard.external_id;

  const existUser = await getUserById(userId);

  if (!existUser) {
    res.status(404).json("User not found!");
  }

  const cusId = existUser.customer_id;

  await stripe.customers.deleteSource(cusId, cardExtId);

  await deletePayment(cardExtId, userId);

  res.status(200).json(`Card was deleted successfully!`);
  res.end();
}

export async function handlePayForOrder(
  req: express.Request,
  res: express.Response
) {
  const user = getUserFromCookies(req, res);
  // @ts-ignore
  const userId = user.subject.id;

  const order = await checkingExistOrder(userId);

  const existUser = await getUserById(userId);

  const cusId = existUser.customer_id;

  const cards = await getPaymentMethod(userId);

  const price = 20;

  const charge = await stripe.charges.create({
    source: cards.external_id,
    amount: 2000,
    currency: "usd",
    // @ts-ignore
    description: `User with email: ${user.subject.email} paid $${price} for order - ${order.id}!`,
    customer: cusId,
  });

  const transactionData = {
    charge_id: charge.id,
    amount: price,
    status: "Paid",
    order_id: order.id,
    card_id: cards.id,
  };

  await generateTransaction(transactionData);

  const status = {
    status: "Paid",
    paid_at: new Date(),
    updated_at: new Date(),
  };

  await updateStatusOrder(order.user_id, { ...status });

  res.json(charge);
  res.end();
}

export async function handleRefundForOrder(
  req: express.Request,
  res: express.Response
) {

  const userId = req.params.id

  const order = await checkingPaidOrder(userId);

  const cards = await getPaymentMethod(userId);

  const paymentCharge = await getChargeForRefund(order.id, cards.id);

  const refund = await stripe.refunds.create({
    charge: paymentCharge.charge_id,
  });

  const statusTransaction = {
    status: "Refund",
    updated_at: new Date(),
  };

  await updateTransaction(paymentCharge.order_id, { ...statusTransaction });

  const statusOrder = {
    status: "Cancelled",
    updated_at: new Date(),
  };
  await updateStatusOrder(order.user_id, { ...statusOrder });

  await deleteOrder (userId)

  res.json(refund);
  res.end();
}
