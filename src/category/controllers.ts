import {
  getCategories,
  getCategoryById,
  createCategory,
  updateCategory,
  deleteCategory,
  deleteCategories,
} from "./services";
import express from "express";

export async function handleCategoriesGet(
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) {
  try {
    const allCategories = await getCategories();
    if (allCategories.length === 0) {
      res.json("Products not found!");
    } else {
      res.json(allCategories);
    }
    res.status(200);
  } catch (error) {
    res.status(404);
  }
  next();
}

export async function handleCategoryGet(
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) {
  try {
    const categoryID = req.params.id;
    const someCategory = await getCategoryById(categoryID);
    res.json(someCategory);
    res.status(200);
  } catch (error) {
    res.status(404);
  }
  next();
}

export async function handleCategoryCreate(
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) {
  try {
    const newCategory = req.body;
    await createCategory(newCategory);
    res.json(newCategory);
    res.status(200);
  } catch (error) {
    res.status(404);
  }
  next();
}

export async function handleCategoryUpdate(
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) {
  try {
    const categoryID = req.params.id;
    const updCategory = req.body;
    await updateCategory(categoryID, { ...updCategory });
    res.json(updCategory);
    res.status(200);
  } catch (error) {
    res.status(404);
  }
  next();
}

export async function handleCategoryDelete(
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) {
  try {
    const categoryID = req.params.id;
    await deleteCategory(categoryID);
    res.json(`Category with id ${categoryID} has been deleted!`);
    res.status(200);
  } catch (error) {
    res.status(404);
  }
  next();
}

export async function handleCategoriesDelete(
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) {
  try {
    await deleteCategories();
    res.json(`All categories have been deleted!`);
    res.status(200);
  } catch (error) {
    res.status(404);
  }
  next();
}
