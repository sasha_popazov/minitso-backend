const db = require("../db/models");

export async function getCategories() {
    try {
      return await db.category.findAll();
    } catch (err) {
        console.error('Table "product" is empty!');
    }
}

export async function getCategoryById(id: string) {
    try {
        const { dataValues } = await db.category.findOne({ where: { id } });
        return dataValues;
    } catch (err) {
        console.error("This product does not exist!");
    }
}

export async function createCategory(category: object) {
    try {
        const { dataValues } = await db.category.create({ ...category });
        return dataValues;
    } catch (err) {
        console.error("Product not created");
    }
}

export async function updateCategory(id:string, category:object) {
    try {
        const { dataValues } = await db.category.update({ ...category }, { where: { id } });
        return dataValues;
    } catch (err) {
        console.error("Product id not updated");
    }
}

export async function deleteCategory(id:string) {
    try {
        const { dataValues } = await db.category.destroy({ where: { id } });
        return dataValues;
    } catch (err) {
        console.error("Product was not deleted");
    }
}

export async function deleteCategories()  {
    try {
        const { dataValues }  = await db.category.destroy({where: {}});
        return dataValues;
    } catch (err) {
        console.error("Products were not deleted");
    }
}