import {
    getCategories,
    getCategoryById,
    createCategory,
    updateCategory,
    deleteCategory,
    deleteCategories,
} from "./services";

jest.mock("../db/models", () => ({
    category: {
        findAll: jest.fn(),
        findOne: jest.fn().mockReturnValue( {}),
        create: jest.fn().mockReturnValue({}),
        update: jest.fn().mockReturnValue({}),
        destroy: jest.fn().mockReturnValue({}),
    },
}));

const db = require("../db/models");

describe("Category service", () => {
    describe("getCategories", () => {
        test("should call findAll method", async () => {
            await getCategories();
            expect(db.category.findAll).toHaveBeenCalled();
            expect(db.category.findAll).toBeCalledTimes(1)
        });
    });

    describe("getCategoryById", () => {
        test("should call findOne method", async () => {
            const id = "1";
            await getCategoryById(id);
            expect(db.category.findOne).toHaveBeenCalledWith({ where: { id: "1" } });
        });
    });

    describe("createCategory", () => {
        test("should call create method", async () => {
            const category = {
                id: "4",
                name: "Drinks",
            };
            await createCategory(category);
            expect(db.category.create).toHaveBeenCalledWith({
                id: "4",
                name: "Drinks",
            });
            expect(db.category.create).not.toBeNull();
        });
    });

    describe("updateCategory", () => {
        test("should call update method", async () => {
            const id = "2"
            const category = {
                name: "Delicious soups",
            };
            await updateCategory(id, {...category});
            expect(db.category.update).toHaveBeenCalledWith( category , { where: { id: "2" } });
        });
    });

    describe("deleteCategory", () => {
        test("should call destroy method", async () => {
            const id = "1";
            await deleteCategory(id);
            expect(db.category.destroy).toHaveBeenCalledWith({ where: { id: "1" } });
        });
    });

    describe("deleteCategories", () => {
        test("should call destroy method", async () => {
            await deleteCategories();
            expect(db.category.destroy).toHaveBeenCalled();
        });
    });
});
