import { Router } from "express";
import {
  handleCategoriesGet,
  handleCategoryGet,
  handleCategoryCreate,
  handleCategoryUpdate,
  handleCategoryDelete,
  handleCategoriesDelete,
} from "./controllers";
import { verifyRole } from "../middlewares/verifyRole";

const categoryRoutes = Router();

categoryRoutes.use(verifyRole(1));

categoryRoutes.get("/", handleCategoriesGet);
categoryRoutes.get("/:id", handleCategoryGet);
categoryRoutes.post("/", handleCategoryCreate);
categoryRoutes.put("/:id", handleCategoryUpdate);
categoryRoutes.delete("/:id", handleCategoryDelete);
categoryRoutes.delete("/", handleCategoriesDelete);

export default categoryRoutes;
