import {
    handleCategoryCreate,
    handleCategoryDelete,
    handleCategoryGet,
    handleCategoriesDelete,
    handleCategoriesGet,
    handleCategoryUpdate,
} from "./controllers";

jest.mock("./services", () => ({
    getCategories: jest.fn().mockReturnValue([
        { id: "1", name: "Pizzas"},
        { id: "2", name: "Soups"},
        { id: "3", name: "Main dishes"},
    ]),
    getCategoryById: jest.fn().mockReturnValue({ id: "1", name: "Pizzas"}),
    createCategory: jest.fn().mockReturnValue({ id: "2", name: "Soups"}),
    updateCategory: jest.fn().mockReturnValue({ id: "3", name: "Main dishes"}),
    deleteCategory: jest.fn().mockReturnValue({ id: "3", name: "Main dishes"}),
    deleteCategories: jest.fn().mockReturnValue({})
}));

const mockResponse = () => {
    return {
        json: jest.fn(),
        status: jest.fn(),
    };
};

const mockNext = () => {
    return jest.fn();
};

describe("Category controller", () => {
    describe("handleCategoriesGet", () => {
        test("should call res, next methods", async () => {
            const req = {}
            const res = mockResponse();
            const next = mockNext();

            // @ts-ignore
            await handleCategoriesGet(req, res, next);

            expect(res.json).toHaveBeenCalledWith([
                { id: "1", name: "Pizzas"},
                { id: "2", name: "Soups"},
                { id: "3", name: "Main dishes"}
            ]);
            expect(res.status).toHaveBeenCalledWith(200);
            expect(next).toHaveBeenCalled();
        });
    });

    describe("handleCategoryGet", () => {
        test("should call res, next methods", async () => {
            const req = {params:{id:"1"}}
            const res = mockResponse();
            const next = mockNext();

            // @ts-ignore
            await handleCategoryGet(req, res, next);

            expect(res.json).toHaveBeenCalledWith(
                {id: "1", name: "Pizzas"});
            expect(res.status).toHaveBeenCalledWith(200);
            expect(next).toHaveBeenCalled();
        });
    });

    describe("handleCategoryCreate", () => {
        test("should call res, next methods", async () => {
            const req = {body:{name: "Soups"}}
            const res = mockResponse();
            const next = mockNext();

            // @ts-ignore
            await handleCategoryCreate(req, res, next);

            expect(res.json).toHaveBeenCalledWith({
                name: "Soups",
            });
            expect(res.status).toHaveBeenCalledWith(200);
            expect(next).toHaveBeenCalled();
        });
    });

    describe("handleCategoryUpdate", () => {
        test("should call res, next methods", async () => {
            const req = {params:{id:"3"}, body:{name: "Main dishes"}}
            const res = mockResponse();
            const next = mockNext();


            // @ts-ignore
            await handleCategoryUpdate(req, res, next);

            expect(res.json).toHaveBeenCalledWith({
                name: "Main dishes"
            });
            expect(res.status).toHaveBeenCalledWith(200);
            expect(next).toHaveBeenCalled();
        });
    });

    describe("handleCategoryDelete", () => {
        test("should call res, next methods", async () => {
            const req = {params:{id:"1"}}
            const res = mockResponse();
            const next = mockNext();


            // @ts-ignore
            await handleCategoryDelete(req, res, next);

            expect(res.json).toHaveBeenCalledWith(`Category with id 1 has been deleted!`);
            expect(res.status).toHaveBeenCalledWith(200);
            expect(next).toHaveBeenCalled();
        });
    });

    describe("handleCategoriesDelete", () => {
        test("should call res, next methods", async () => {
            const req = {}
            const res = mockResponse();
            const next = mockNext();


            // @ts-ignore
            await handleCategoriesDelete(req, res, next);

            expect(res.json).toHaveBeenCalledWith(`All categories have been deleted!`);
            expect(res.status).toHaveBeenCalledWith(200);
            expect(next).toHaveBeenCalled();
        });
    });

});

