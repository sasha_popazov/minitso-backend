import {
  addModToProductToOrder, addProductToOrder,
  checkingExistOrder,
  checkingStatusOrder,
  checkModifier, createOrder,
  deleteModFromProductFromOrder,
  deleteProductFromOrder,
  getExistUserOrder, getOldOrders,
  getOrderedProduct,
  getOrderProductsAsAdmin,
  getOrderProductsAsUser,
  getUserOrderStatus,
  updateStatusOrder,
} from "./services";
import express from "express";
import { getUserFromCookies } from "../middlewares/verify";

export async function handleAddProductToOrder(
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) {
  try {
    const user = await getUserFromCookies(req, res);

    // @ts-ignore
    const userId = user.subject.id;

    const userOrder = await checkingExistOrder(userId);

    if (!userOrder) {
      const newOrder = {
        user_id: userId,
      };
      await createOrder(newOrder);
    }

    const product = req.body;
    const order = await getExistUserOrder(userId);

    const productToOrder = {
      product_id: product.id,
      order_id: order.id,
    };

    const orderedProduct = await addProductToOrder(productToOrder);

    const statusOrder = await checkingStatusOrder(userId);

    if (statusOrder) {
      // adding first product to cart
      const newStatus = {
        status: "Processing",
        delivery_time: new Date(),
        updated_at: new Date(),
      };
      await updateStatusOrder(order.user_id, { ...newStatus });
      res.json(orderedProduct);
    }
    res.json(orderedProduct);
  } catch (error) {
    res.status(404);
  }
  next();
}

export async function handleDelProductFromOrder(
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) {
  try {
    const user = await getUserFromCookies(req, res);

    // @ts-ignore
    const userId = user.subject.id;

    const product = req.body;

    const userOrder = await getExistUserOrder(userId);

    await deleteProductFromOrder(product.id, userOrder.id);

    const newStatus = {
      status: "Created",
      delivery_time: null,
      updated_at: new Date(),
    };
    await updateStatusOrder(userOrder.user_id, { ...newStatus });

    res.json(`Product was deleted successfully!`);
  } catch (error) {
    res.status(404);
  }
  next();
}

export async function handleAddModToProductToOrder(
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) {
  try {
    const user = await getUserFromCookies(req, res);

    // @ts-ignore
    const userId = user.subject.id;

    const modifier = req.body;

    const userOrder = await getExistUserOrder(userId);

    const orderedProduct = await getOrderedProduct(userOrder.id);

    if (!orderedProduct) {
      res.status(404).json("You are doing something wrong!");
    }

    const existModifier = await checkModifier(
      orderedProduct.product_id,
      modifier.id
    );

    if (!existModifier) {
      res.status(404).json("Modifier for this product not found!");
    } else {
      const productToModification = {
        modifier_id: modifier.id,
        products_to_order_id: orderedProduct.id,
      };

      const modifiedProduct = await addModToProductToOrder(
        productToModification
      );
      res.json(modifiedProduct);
    }
  } catch (error) {
    res.status(404);
  }
  next();
}

export async function handleDelModifierFromOrder(
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) {
  try {
    const user = await getUserFromCookies(req, res);

    // @ts-ignore
    const userId = user.subject.id;

    const modifier = req.body;

    const userOrder = await getExistUserOrder(userId);

    const orderedProduct = await getOrderedProduct(userOrder.id);

    await deleteModFromProductFromOrder(orderedProduct.id, modifier.id);

    res.json(`Modifier was deleted successfully!`);
  } catch (error) {
    res.status(404);
  }
  next();
}

export async function handleViewAsAdminUsersOrders(
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) {
  try {

    const user = await getUserFromCookies(req, res);
    // @ts-ignore
    const userId = user.subject.id;

    const product = await getOrderProductsAsAdmin(userId);
    res.json(product);
  } catch (error) {
    res.status(404);
  }
  next();
}

export async function handleViewAsUserOrder(
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) {
  try {
    const user = await getUserFromCookies(req, res);
    // @ts-ignore
    const userId = user.subject.id;
    const existUser = await getExistUserOrder(userId);
    if (!existUser) {
      res.json("Users not found!");
    } else {
      const product = await getOrderProductsAsUser(userId);
      res.json(product);
    }
  } catch (error) {
    res.status(404);
  }
  next();
}

export async function handleViewCurrentStatusOrder(
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) {
  try {
    const user = await getUserFromCookies(req, res);

    // @ts-ignore
    const userId = user.subject.id;

    const orderId = req.params.id;

    const orderStatus = await getUserOrderStatus(userId, orderId);
    if (!orderStatus) {
      res.json("Order not found!");
    } else {
      res.json(`Your last order has status: ${orderStatus.status}`);
    }
  } catch (error) {
    res.status(404);
  }
  next();
}

export async function handleViewPaidOrders(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
) {
  try {
    const user = await getUserFromCookies(req, res);

    // @ts-ignore
    const userId = user.subject.id;

    const oldOrders = await getOldOrders(userId)

    if (!oldOrders) {
      res.status(404).json("You haven't made any orders yet!");
    } else {
      res.status(200).json(oldOrders);
    }
  } catch (error) {
    res.status(404);
  }
  next();
}
