import { Router } from "express";
import {
  handleAddModToProductToOrder,
  handleAddProductToOrder,
  handleDelModifierFromOrder,
  handleDelProductFromOrder,
  handleViewAsAdminUsersOrders,
  handleViewAsUserOrder, handleViewCurrentStatusOrder, handleViewPaidOrders,
} from "./controllers";
import {verifyRole} from "../middlewares/verifyRole";

const orderRoutes = Router();

orderRoutes.post("/prod", handleAddProductToOrder);
orderRoutes.delete("/prod", handleDelProductFromOrder);
orderRoutes.post("/mod", handleAddModToProductToOrder);
orderRoutes.delete("/mod", handleDelModifierFromOrder);
orderRoutes.get("/view", handleViewAsUserOrder);
orderRoutes.get("/view/:id", handleViewCurrentStatusOrder);
orderRoutes.get("/old", handleViewPaidOrders);

orderRoutes.use(verifyRole(1));
orderRoutes.get("/:id", handleViewAsAdminUsersOrders);

export default orderRoutes;
