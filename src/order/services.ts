const db = require("../db/models");
const { Op } = require("sequelize");

export async function checkingExistOrder(user_id: string) {
  try {
    const status = await db.order.findOne({
      where: {
        [Op.or]: [{ status: "Created" }, { status: "Processing" }],
        user_id: user_id,
      },
    });
    return status;
  } catch (err) {
    throw new Error("Checking failed!");
  }
}

export async function checkingPaidOrder(user_id: string) {
  try {
    const status = await db.order.findOne({
      where: {
        status: "Paid",
        user_id: user_id,
      },
    });
    return status;
  } catch (err) {
    throw new Error("Checking failed!");
  }
}

export async function createOrder(order: object) {
  try {
    const { dataValues } = await db.order.create({ ...order });
    return dataValues;
  } catch (err) {
    throw new Error("Order not created");
  }
}

export async function deleteOrder(user_id: string) {
  try {
    const { dataValues } = await db.order.destroy({
      where: { status: "Cancelled", user_id: user_id },
    });
    return dataValues;
  } catch (err) {
    throw new Error("Order not created");
  }
}

export async function checkingStatusOrder(user_id: string) {
  try {
    const check = await db.order.findOne({
      where: {
        user_id: user_id,
        status: "Created",
      },
    });
    return check;
  } catch (err) {
    throw new Error("Order not created");
  }
}

export async function updateStatusOrder(user_id: string, order: object) {
  try {
    const { dataValues } = await db.order.update(
      { ...order },
      { where: { user_id: user_id } }
    );
    return dataValues;
  } catch (err) {
    throw new Error("Order is not updated!");
  }
}

export async function getExistUserOrder(user_id: string) {
  try {
    const exist = await db.order.findOne({
      where: { user_id: user_id },
    });
    return exist;
  } catch (err) {
    throw new Error("This order does not exist!");
  }
}

export async function addProductToOrder(orderedProduct: object) {
  try {
    const { dataValues } = await db.products_to_order.create({
      ...orderedProduct,
    });
    return dataValues;
  } catch (err) {
    throw new Error("Product is not added to order!");
  }
}

export async function deleteProductFromOrder(product: string, order: string) {
  try {
    const { dataValues } = await db.products_to_order.destroy({
      where: {
        product_id: product,
        order_id: order,
      },
    });
    return dataValues;
  } catch (err) {
    throw new Error("Product is not deleted from order!");
  }
}

export async function getOrderedProduct(userOrder_id: string) {
  try {
    const exist = await db.products_to_order.findOne({
      where: { order_id: userOrder_id },
      order: [["id", "DESC"]],
    });
    return exist;
  } catch (err) {
    throw new Error("This order does not exist!");
  }
}

export async function checkModifier(product_id: string, modifier_id: string) {
  try {
    const exMod = await db.modified_products.findOne({
      where: {
        product_id: product_id,
        modifier_id: modifier_id,
      },
    });
    return exMod;
  } catch (err) {
    throw new Error("Something went wrong!");
  }
}

export async function addModToProductToOrder(modified_order: object) {
  try {
    const { dataValues } = await db.modifiers_to_order.create({
      ...modified_order,
    });

    return dataValues;
  } catch (err) {
    throw new Error("Product is not added to order!");
  }
}

export async function deleteModFromProductFromOrder(
  product_id: any,
  modifier_id: any
) {
  try {
    const { dataValues } = await db.modifiers_to_order.destroy({
      where: {
        products_to_order_id: product_id,
        modifier_id: modifier_id,
      },
    });
    return dataValues;
  } catch (err) {
    throw new Error("Product is not deleted from order!");
  }
}

export async function getOrderProductsAsAdmin(user_id: string) {
  try {
    const { dataValues } = await db.order.findOne({
      where: { user_id: user_id },
      include: {
        model: db.products_to_order,
        attributes: ["id"],
        include: [
          {
            model: db.product,
          },
          {
            model: db.modifiers_to_order,
            attributes: ["id"],
            include: {
              model: db.modifier,
            },
          },
        ],
      },
    });
    return dataValues;
  } catch (err) {
    throw new Error("This order does not exist!");
  }
}

export async function getOrderProductsAsUser(user_id: string) {
  try {
    const { dataValues } = await db.order.findOne({
      attributes: ["status"],
      where: { user_id: user_id },
      include: {
        model: db.products_to_order,
        attributes: ["id"],
        include: [
          {
            model: db.product,
            attributes: ["id", "name", "price", "description"],
          },
          {
            model: db.modifiers_to_order,
            attributes: ["id"],
            include: {
              model: db.modifier,
              attributes: ["id", "name", "price"],
            },
          },
        ],
      },
    });
    return dataValues;
  } catch (err) {
    throw new Error("This order does not exist!");
  }
}

export async function getUserOrderStatus(user_id: string, order_id: string) {
  try {
    const exist = await db.order.findOne({
      where: {
        user_id: user_id,
        id: order_id,
      },
    });
    return exist;
  } catch (err) {
    throw new Error("This order does not exist!");
  }
}

export async function getOldOrders(user_id: string) {
  try {
    const old = await db.order.findAll({
      attributes: ["status"],
      where: {
        status: "Paid",
        user_id: user_id,
      },
      include: {
        model: db.products_to_order,
        attributes: ["id"],
        include: [
          {
            model: db.product,
            attributes: ["id", "name", "price", "description"],
          },
          {
            model: db.modifiers_to_order,
            attributes: ["id"],
            include: {
              model: db.modifier,
              attributes: ["id", "name", "price"],
            },
          },
        ],
      },
    });
    return old
  } catch (err) {
    throw new Error("Checking failed!");
  }
}