import {getUsers, getUserById, createUser, updateUser, deleteUser, deleteUsers, getByEmail} from "./services";

jest.mock("../db/models", () => ({
  user: {
    findAll: jest.fn(),
    findOne: jest.fn().mockReturnValue( {}),
    create: jest.fn().mockReturnValue({}),
    update: jest.fn().mockReturnValue({}),
    destroy: jest.fn().mockReturnValue({}),
  },
}));

const db = require("../db/models");

describe("User service", () => {
  describe("getUsers", () => {
    test("should call findAll method", async () => {
      await getUsers();
      expect(db.user.findAll).toHaveBeenCalled();
    });
  });

  describe("getUserById", () => {
    test("should call findOne method", async () => {
      const id = "2";
      await getUserById(id);
      expect(db.user.findOne).toHaveBeenCalledWith({ where: { id: "2" } });
    });
  });

  describe("createUser", () => {
    test("should call create method", async () => {
      const user = {
        id: "1",
        first_name: "Sam",
        last_name: "Winchester"
      };
      await createUser(user);
      expect(db.user.create).toHaveBeenCalledWith({
        id: "1",
        first_name: "Sam",
        last_name: "Winchester"
      });
    });
  });

  describe("updateUser", () => {
    test("should call update method", async () => {
      const id = "2"
      const user = {
        first_name: "Sam",
        last_name: "Winchester"
      };
      await updateUser(id, {...user});
      expect(db.user.update).toHaveBeenCalledWith( user , { where: { id: "2" } });
    });
  });

  describe("deleteUser", () => {
    test("should call destroy method", async () => {
      const id = "2";
      await deleteUser(id);
      expect(db.user.destroy).toHaveBeenCalledWith({ where: { id: "2" } });
    });
  });

  describe("deleteUsers", () => {
    test("should call destroy method", async () => {
      await deleteUsers();
      expect(db.user.destroy).toHaveBeenCalled();
    });
  });

  describe("getByEmail", () => {
    test("should call finOne method", async () => {
      const email = 'test@gmail.com'
      await getByEmail(email);
      expect(db.user.findOne).toHaveBeenCalledWith({where: { email: "test@gmail.com" } });
    });
  });
});
