const db = require("../db/models");

export async function getUsers() {
    try {
      return await db.user.findAll();
    } catch (err) {
        throw new Error('Table "user" is empty!');
    }
}

export async function getUserById(user_id: string) {
    try {
        const { dataValues } = await db.user.findOne({ where: { id: user_id } });
        return dataValues;
    } catch (err) {
        throw new Error("This user does not exist!");
    }
}

export async function createUser(user: object) {
    try {
        const { dataValues } = await db.user.create({ ...user });
        return dataValues;
    } catch (err) {
        throw new Error("User not created");
    }
}

export async function updateUser(id:string, user:object) {
    try {
        const { dataValues } = await db.user.update({ ...user }, { where: { id } });
        return dataValues;
    } catch (err) {
        throw new Error("User id not updated");
    }
}

export async function deleteUser  (id:string) {
    try {
        const { dataValues } = await db.user.destroy({ where: { id } });
        return dataValues;
    } catch (err) {
        throw new Error("User was not deleted");
    }
}

export async function deleteUsers()  {
    try {
        const { dataValues }  = await db.user.destroy({where: {}});
        return dataValues;
    } catch (err) {
        throw new Error("Users were not deleted");
    }
}

export async function getByEmail(email: string) {
    try {
        const { dataValues }  = await db.user.findOne({ where: { email } });
        return dataValues;
    } catch (err) {
    }
}