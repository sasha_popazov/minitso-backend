import { Router } from "express";
import {
  handleUsersGet,
  handleUserGet,
  handleUserCreate,
  handleUserUpdate,
  handleUserDelete,
  handleUsersDelete, handleUserUpdateByAdmin,
} from "./controllers";
import { verifyRole } from "../middlewares/verifyRole";

const userRoutes = Router();

userRoutes.patch("/edit", handleUserUpdate);

userRoutes.use(verifyRole(1));
userRoutes.get("/", handleUsersGet);
userRoutes.get("/:id", handleUserGet);
userRoutes.post("/", handleUserCreate);
userRoutes.patch("/:id", handleUserUpdateByAdmin);
userRoutes.delete("/:id", handleUserDelete);
userRoutes.delete("/", handleUsersDelete);

export default userRoutes;
