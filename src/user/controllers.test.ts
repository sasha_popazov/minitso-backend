import {handleUserCreate, handleUserDelete, handleUsersDelete} from "./controllers";

const { handleUsersGet, handleUserGet } = require("./controllers");

jest.mock("./services", () => ({
  getUsers: jest.fn().mockReturnValue([
    { id: 1, first_name: "Sam", last_name: "Winchester" },
    { id: 2, first_name: "Din", last_name: "Winchester" },
    { id: 3, first_name: "John", last_name: "Winchester" },
  ]),
  getUserById: jest.fn().mockReturnValue({id: "2", first_name: "Din", last_name: "Winchester" }),
  createUser: jest.fn().mockReturnValue({id: "3", first_name: "John", last_name: "Winchester"}),
  updateUser: jest.fn().mockReturnValue({first_name: "Sam", last_name: "Winchester"}),
  deleteUser: jest.fn().mockReturnValue({id: "1", first_name: "Sam", last_name: "Winchester"}),
  deleteUsers: jest.fn().mockReturnValue({})
}));

const mockResponse = () => {
  return {
    json: jest.fn(),
    status: jest.fn(),
  };
};

const mockNext = () => {
  return jest.fn();
};

describe("User controller", () => {
  describe("handleUsersGet", () => {
    test("should call res, next methods", async () => {
      const req = {}
      const res = mockResponse();
      const next = mockNext();

      await handleUsersGet(req, res, next);

      expect(res.json).toHaveBeenCalledWith([
        { id: 1, first_name: "Sam", last_name: "Winchester" },
        { id: 2, first_name: "Din", last_name: "Winchester" },
        { id: 3, first_name: "John", last_name: "Winchester" },
      ]);
      expect(res.status).toHaveBeenCalledWith(200);
      expect(next).toHaveBeenCalled();
    });
  });

  describe("handleUserGet", () => {
    test("should call res, next methods", async () => {
      const req = {params:{id:"2"}}
      const res = mockResponse();
      const next = mockNext();

      await handleUserGet(req, res, next);

      expect(res.json).toHaveBeenCalledWith(
          {id: "2", first_name: "Din", last_name: "Winchester"});
      expect(res.status).toHaveBeenCalledWith(200);
      expect(next).toHaveBeenCalled();
    });
    test("should catch errors", async () => {
      const req = {};
      const res = mockResponse();
      const next = mockNext();

      await handleUserGet(req, res, next);

      expect(res.status).toHaveBeenCalledWith(404);
      expect(next).toHaveBeenCalled();
    });
  });

  describe("handleUserCreate", () => {
    test("should call res, next methods", async () => {
      const req = {body:{first_name: "John", last_name: "Winchester"}}
      const res = mockResponse();
      const next = mockNext();

      // @ts-ignore
      await handleUserCreate(req, res, next);

      expect(res.json).toHaveBeenCalledWith({
        first_name: "John",
        last_name: "Winchester"
      });
      expect(res.status).toHaveBeenCalledWith(200);
      expect(next).toHaveBeenCalled();
    });
  });


  describe("handleUserDelete", () => {
    test("should call res, next methods", async () => {
      const req = {params:{id:"1"}}
      const res = mockResponse();
      const next = mockNext();


      // @ts-ignore
      await handleUserDelete(req, res, next);

      expect(res.json).toHaveBeenCalledWith(`User with id 1 has been deleted!`);
      expect(res.status).toHaveBeenCalledWith(200);
      expect(next).toHaveBeenCalled();
    });
  });

  describe("handleUsersDelete", () => {
    test("should call res, next methods", async () => {
      const req = {}
      const res = mockResponse();
      const next = mockNext();


      // @ts-ignore
      await handleUsersDelete(req, res, next);

      expect(res.json).toHaveBeenCalledWith(`All users have been deleted!`);
      expect(res.status).toHaveBeenCalledWith(200);
      expect(next).toHaveBeenCalled();
    });
  });

});

