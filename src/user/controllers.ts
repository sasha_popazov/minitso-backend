import {
  getUsers,
  getUserById,
  createUser,
  updateUser,
  deleteUser,
  deleteUsers, getByEmail,
} from "./services";
import express from "express";
import {getUserFromCookies} from "../middlewares/verify";

export async function handleUsersGet(
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) {
  try {
    const allUsers = await getUsers();
    if (allUsers.length === 0) {
      res.json("Users not found!");
    } else {
      res.json(allUsers);
    }
    res.status(200);
  } catch (error) {
    res.status(404);
  }
  next();
}

export async function handleUserGet(
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) {
  try {
    const userID = req.params.id;
    const someUser = await getUserById(userID);
    res.json(someUser);
    res.status(200);
  } catch (error) {
    res.status(404);
  }
  next();
}

export async function handleUserCreate(
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) {
  try {
    const newUser = req.body;
    await createUser(newUser);
    res.json(newUser);
    res.status(200);
  } catch (error) {
    res.status(404);
  }
  next();
}

export async function handleUserUpdate(
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) {
  try {
    const user = getUserFromCookies(req,res);
    // @ts-ignore
    const userID = user.subject.id;
    const updUser = req.body;
    const candidate = await getByEmail(updUser.email);
    if (candidate) {
      res.status(404).json("Email is not update! Some user already have this one!");
    }
    await updateUser(userID, { ...updUser });
    res.json(updUser);
    res.status(200);
  } catch (error) {
    res.status(404);
  }
  next();
}

export async function handleUserUpdateByAdmin(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
) {
  try {

    const userID = req.params.id

    const updUser = req.body;
    const candidate = await getByEmail(updUser.email);
    if (candidate) {
      res.status(404).json("Email is not update! Some user already have this one!");
    }
    await updateUser(userID, { ...updUser });
    res.json(updUser);
    res.status(200);
  } catch (error) {
    res.status(404);
  }
  next();
}

export async function handleUserDelete(
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) {
  try {
    const userID = req.params.id;
    await deleteUser(userID);
    res.json(`User with id ${userID} has been deleted!`);
    res.status(200);
  } catch (error) {
    res.status(404);
  }
  next();
}

export async function handleUsersDelete(
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) {
  try {
    await deleteUsers();
    res.json(`All users have been deleted!`);
    res.status(200);
  } catch (error) {
    res.status(404);
  }
  next();
}
