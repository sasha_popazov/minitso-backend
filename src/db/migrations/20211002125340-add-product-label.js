'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('products', 'label_id', {
      allowNull: true,
      type: Sequelize.INTEGER,
      references: { model: 'labels', key: 'id' },
      onUpdate: 'CASCADE',
      onDelete: 'SET NULL',
    });
  },

  down: async (queryInterface) => {
    await queryInterface.removeColumn('products', 'label_id');
  }
};