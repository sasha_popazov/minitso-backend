"use strict";
const bcrypt = require("bcrypt");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert("users", [
      {
        first_name: "Admin",
        last_name: "Admin",
        email: "Admin@gmail.com",
        password: await bcrypt.hash("admin", 10),
        phone_number: "777777777",
        role_id: 1,
        created_at: Sequelize.literal("CURRENT_TIMESTAMP"),
        updated_at: Sequelize.literal("CURRENT_TIMESTAMP"),
      },
      {
        first_name: "Den",
        last_name: "Green",
        email: "DG@gmail.com",
        password: await bcrypt.hash("password", 10),
        phone_number: "0847544660",
        role_id: 2,
        address_id: 1,
        created_at: Sequelize.literal("CURRENT_TIMESTAMP"),
        updated_at: Sequelize.literal("CURRENT_TIMESTAMP"),
      },
    ]);
  },
  down: async (queryInterface) => {
    await queryInterface.bulkDelete("users", null, {});
  },
};
