"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert("addresses", [
      {
        street: "Wall street",
        building: 44,
        created_at: Sequelize.literal("CURRENT_TIMESTAMP"),
        updated_at: Sequelize.literal("CURRENT_TIMESTAMP"),
      },
    ]);
  },
  down: async (queryInterface) => {
    await queryInterface.bulkDelete("addresses", null, {});
  },
};
