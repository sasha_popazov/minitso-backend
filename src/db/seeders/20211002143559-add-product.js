'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('products',[
      {
        name: "Four Cheese",
        price: 5.8,
        description: "Pizza Four cheeses is distinguished by an amazing taste range, as it contains the following " +
            "cheeses: parmesan and with noble mold.",
        image: "https://test-download-boilerplate.s3.us-east-2.amazonaws.com/Four+Cheese.jpg",
        label_id: 1,
        category_id: 1,
        created_at: Sequelize.literal('CURRENT_TIMESTAMP'),
        updated_at: Sequelize.literal('CURRENT_TIMESTAMP')
      },
      {
        name: "Diablo",
        price: 6.6,
        description: "Spicy pizza Diablo will appeal to those who crave peppercorns. Spicy salami, chili " +
            "peppers, hunting sausages, smoked cheese, chicken fillet, srirachi sauce will impress even the most " +
            "discerning gourmets.",
        image: "https://test-download-boilerplate.s3.us-east-2.amazonaws.com/Diablo.jpg",
        label_id: 1,
        category_id: 1,
        created_at: Sequelize.literal('CURRENT_TIMESTAMP'),
        updated_at: Sequelize.literal('CURRENT_TIMESTAMP')
      },
      {
        name: "Fire Carolina",
        price: 10.3,
        description: "Here is the hottest pizza in the city Fire Carolina. Pizza for the brave and " +
            "fearless spicy lovers!",
        image: "https://test-download-boilerplate.s3.us-east-2.amazonaws.com/Fire+Carolina.jpg",
        label_id: 1,
        category_id: 1,
        created_at: Sequelize.literal('CURRENT_TIMESTAMP'),
        updated_at: Sequelize.literal('CURRENT_TIMESTAMP')
      },
      {
        name: "Pickled soup",
        price: 2.7,
        description: "Pickled soup with smoked meat is a hearty and nutritious first course, thick, rich " +
            "and aromatic soup.",
        image: "https://test-download-boilerplate.s3.us-east-2.amazonaws.com/Pickled+soup.jpg",
        label_id: 1,
        category_id: 2,
        created_at: Sequelize.literal('CURRENT_TIMESTAMP'),
        updated_at: Sequelize.literal('CURRENT_TIMESTAMP')
      },
      {
        name: "Ukrainian borsch",
        price: 2.8,
        description: "What could be better for lunch than rich Ukrainian borsch? Rich broth with beef and beetroot base, " +
            "tender yet crispy cabbage and soft potato pieces.",
        image: "https://test-download-boilerplate.s3.us-east-2.amazonaws.com/Ukrainian+borsch.jpg",
        label_id: 1,
        category_id: 2,
        created_at: Sequelize.literal('CURRENT_TIMESTAMP'),
        updated_at: Sequelize.literal('CURRENT_TIMESTAMP')
      },
      {
        name: "Salmon ear",
        price: 3.2,
        description: "Salmon fish soup is a rich, delicious, nutritious first course with a delicate taste.",
        image: "https://test-download-boilerplate.s3.us-east-2.amazonaws.com/Salmon+ear.jpg",
        label_id: 1,
        category_id: 2,
        created_at: Sequelize.literal('CURRENT_TIMESTAMP'),
        updated_at: Sequelize.literal('CURRENT_TIMESTAMP')
      },
      {
        name: "Chicken with vegetables and pancetta",
        price: 5.3,
        description: "The most delicate chicken flesh is wrapped in pancetta and baked, making it " +
            "incredibly tender, juicy and aromatic.",
        image: "https://test-download-boilerplate.s3.us-east-2.amazonaws.com/Chicken+with+vegetables+and+pancetta.jpg",
        label_id: 1,
        category_id: 3,
        created_at: Sequelize.literal('CURRENT_TIMESTAMP'),
        updated_at: Sequelize.literal('CURRENT_TIMESTAMP')
      },
      {
        name: "Pork medallions with mushroom sauce",
        price: 6.5,
        description: "Pork with mushroom sauce and mashed potatoes can rightfully become one of the " +
            "favorites of the festive table or a hearty lunch.",
        image: "https://test-download-boilerplate.s3.us-east-2.amazonaws.com/Pork+medallions+with+mushroom+sauce.jpg",
        label_id: 1,
        category_id: 3,
        created_at: Sequelize.literal('CURRENT_TIMESTAMP'),
        updated_at: Sequelize.literal('CURRENT_TIMESTAMP')
      },
      {
        name: "Savory ribs with barbecue sauce",
        price: 10.5,
        description: "Juicy and tender ribs are one of the most popular meat dishes. Our signature barbecue " +
            "sauce will give your baked ribs a special piquancy - we know how to serve them in their best " +
            "possible way to your table.",
        image: "https://test-download-boilerplate.s3.us-east-2.amazonaws.com/Savory+ribs+with+barbecue+sauce.jpg",
        label_id: 1,
        category_id: 3,
        created_at: Sequelize.literal('CURRENT_TIMESTAMP'),
        updated_at: Sequelize.literal('CURRENT_TIMESTAMP')
      },
    ])
  },
  down: async (queryInterface) => {
    await queryInterface.bulkDelete('products', null, {});
  }
};
