"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class products_to_order extends Model {
    static associate(models) {
      // define association here
      // products_to_order.belongsToMany(models.modifier, {
      //   through: "modifiers_to_orders",
      // })
      products_to_order.belongsTo(models.order)
      products_to_order.belongsTo(models.product)
      products_to_order.hasMany(models.modifiers_to_order)
    }
  }
  products_to_order.init(
    {
      quantity: DataTypes.INTEGER,
      order_id: {
        type: DataTypes.INTEGER,
        references: {
          model: "orders",
          key: "id",
          underscored: true,
        },
      },
      product_id: {
        type: DataTypes.INTEGER,
        references: {
          model: "products",
          key: "id",
          underscored: true,
        },
      },
    },
    {
      sequelize,
      modelName: "products_to_order",
      underscored: true,
    }
  );
  return products_to_order;
};
