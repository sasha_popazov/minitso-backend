'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class address extends Model {
    static associate(models) {
      address.hasMany(models.user)
    }
  }
  address.init({
    street: DataTypes.STRING,
    building: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'address',
    underscored: true,
  });
  return address;
};