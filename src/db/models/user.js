"use strict";
const { Model } = require("sequelize");
const bcrypt = require("bcrypt");
module.exports = (sequelize, DataTypes) => {
  class user extends Model {
    static associate(models) {
      user.hasMany(models.order)
      user.hasMany(models.card)
      user.belongsTo(models.role)
      user.belongsTo(models.address)
    }
  }
  user.init(
    {
      first_name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      last_name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      phone_number: DataTypes.INTEGER,
      customer_id: DataTypes.STRING,
      role_id: {
        type: DataTypes.INTEGER,
        allowNull: true,
        defaultValue: 2,
        references: {
          model: "role",
          key: "id",
          underscored: true,
        },
      },
    },
    {
      sequelize,
      modelName: "user",
      underscored: true,
    }
  );
  user.addHook("beforeCreate", async (user) => {
    user.password = await bcrypt.hash(user.password, 10);
  });
  return user;
};
