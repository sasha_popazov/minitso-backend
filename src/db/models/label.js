'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class label extends Model {
    static associate(models) {
      label.hasMany(models.product)
    }
  }
  label.init({
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    image: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'label',
    underscored: true,
  });
  return label;
};