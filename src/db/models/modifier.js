'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class modifier extends Model {
    static associate(models) {
      modifier.belongsToMany(models.product, { through: "modified_products" });
      modifier.hasMany(models.modifiers_to_order);
    }
  }
  modifier.init({
    name: DataTypes.STRING,
    price: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'modifier',
    underscored: true,
  });
  return modifier;
};