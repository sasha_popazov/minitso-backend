"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
    class modified_products extends Model {
        static associate(models) {
        }
    }
    modified_products.init(
        {
            product_id: {
                type: DataTypes.INTEGER,
                references: {
                    model: "products",
                    key: "id",
                    underscored: true,
                },
            },
            modifier_id: {
                type: DataTypes.INTEGER,
                references: {
                    model: "modifiers",
                    key: "id",
                    underscored: true,
                },
            },
        },
        {
            sequelize,
            modelName: "modified_products",
            underscored: true,
        }
    );
    return modified_products;
};
