"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class modifiers_to_order extends Model {
    static associate(models) {
        modifiers_to_order.belongsTo(models.products_to_order)
        modifiers_to_order.belongsTo(models.modifier)
    }
  }
  modifiers_to_order.init(
    {
    quantity: DataTypes.INTEGER,
        products_to_order_id: {
      type: DataTypes.INTEGER,
          references: {
        model: "products_to_orders",
            key: "id",
            underscored: true,
      },
    },
      modifier_id: {
      type: DataTypes.INTEGER,
          references: {
        model: "modifiers",
            key: "id",
            underscored: true,
      },
    },
  },
    {
      sequelize,
      modelName: "modifiers_to_order",
      underscored: true,
    }
  );
  return modifiers_to_order;
};
