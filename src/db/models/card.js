"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class card extends Model {
    static associate(models) {
      card.belongsToMany(models.order, { through: "transactions" });
      card.belongsTo(models.user);
    }
  }
  card.init(
    {
      brand: DataTypes.STRING,
      external_id: DataTypes.STRING,
      user_id: {
        type: DataTypes.INTEGER,
        references: {
          model: "user",
          key: "id",
          underscored: true,
        },
      },
    },
    {
      sequelize,
      modelName: "card",
      underscored: true,
    }
  );
  return card;
};
