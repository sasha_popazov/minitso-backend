'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class order extends Model {
    static associate(models) {
      order.belongsTo(models.user)
      order.belongsToMany(models.card, { through: "transactions" });
      order.hasMany(models.products_to_order)
    }
  }
  order.init({
    status: DataTypes.STRING,
    price: DataTypes.INTEGER,
    delivery_time: DataTypes.DATE,
    paid_at:  DataTypes.DATE,
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: "user",
        key: "id",
        underscored: true,
      },
    },
  }, {
    sequelize,
    modelName: 'order',
    underscored: true,
  });
  return order;
};