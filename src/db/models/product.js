'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class product extends Model {
    static associate(models) {
      product.belongsTo(models.label)
      product.belongsTo(models.category)
      product.belongsToMany(models.modifier, { through: "modified_products" });
      product.hasMany(models.products_to_order);
    }
  }
  product.init({
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    price: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    image: DataTypes.STRING,
    description: DataTypes.STRING,
    label_id : {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: "label",
        key: "id",
        underscored: true,
      }},
    category_id : {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: "category",
        key: "id",
        underscored: true,
      }},
  }, {
    sequelize,
    modelName: 'product',
    underscored: true,
  });
  return product;
};