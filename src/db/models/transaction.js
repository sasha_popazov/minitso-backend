'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class transaction extends Model {
    static associate(models) {
      // define association here
      transaction.belongsTo(models.order)
      transaction.belongsTo(models.card)
    }
  }
  transaction.init({
    charge_id: DataTypes.STRING,
    amount: DataTypes.INTEGER,
    status: DataTypes.STRING,
    order_id: {
      type: DataTypes.INTEGER,
      references: {
        model: "orders",
        key: "id",
        underscored: true,
      },
    },
    card_id: {
      type: DataTypes.INTEGER,
      references: {
        model: "cards",
        key: "id",
        underscored: true,
      },
    },
  }, {
    sequelize,
    modelName: 'transaction',
    underscored: true,
  });
  return transaction;
};