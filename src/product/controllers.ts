import {
  getProducts,
  getProductById,
  createProduct,
  updateProduct,
  deleteProduct,
  deleteProducts,
} from "./services";
import express from "express";

export async function handleProductsGet(
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) {
  try {
    const allProducts = await getProducts();
    if (allProducts.length === 0) {
      res.json("Products not found!");
    } else {
      res.json(allProducts);
    }
    res.status(200);
  } catch (error) {
    res.status(404);
  }
  next();
}

export async function handleProductGet(
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) {
  try {
    const productID = req.params.id;
    const someProduct = await getProductById(productID);
    res.json(someProduct);
    res.status(200);
  } catch (error) {
    res.status(404).json("Product not found!");
  }
  next();
}

export async function handleProductCreate(
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) {
  try {
    const newProducts = req.body;
    await createProduct(newProducts);
    res.json(newProducts);
    res.status(200);
  } catch (error) {
    res.status(404);
  }
  next();
}

export async function handleProductUpdate(
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) {
  try {
    const productID = req.params.id;
    const updProduct = req.body;
    await updateProduct(productID, { ...updProduct });
    res.json(updProduct);
    res.status(200);
  } catch (error) {
    res.status(404);
  }
  next();
}

export async function handleProductDelete(
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) {
  try {
    const productID = req.params.id;
    await deleteProduct(productID);
    res.json(`Product with id ${productID} has been deleted!`);
    res.status(200);
  } catch (error) {
    res.status(404);
  }
  next();
}

export async function handleProductsDelete(
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) {
  try {
    await deleteProducts();
    res.json(`All products have been deleted!`);
    res.status(200);
  } catch (error) {
    res.status(404);
  }
  next();
}
