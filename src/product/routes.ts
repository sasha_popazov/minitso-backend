import { Router } from "express";
import {
  handleProductCreate,
  handleProductDelete,
  handleProductGet,
  handleProductsDelete,
  handleProductsGet,
  handleProductUpdate,
} from "./controllers";
import {verifyRole} from "../middlewares/verifyRole";

const productRoutes = Router();

productRoutes.use(verifyRole(1));

productRoutes.get("/", handleProductsGet);
productRoutes.get("/:id", handleProductGet);
productRoutes.post("/",  handleProductCreate);
productRoutes.put("/:id", handleProductUpdate);
productRoutes.delete("/:id", handleProductDelete);
productRoutes.delete("/", handleProductsDelete);

export default productRoutes;
