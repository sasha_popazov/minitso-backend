const db = require("../db/models");

export async function getProducts() {
    try {
        return await db.product.findAll();
    } catch (err) {
        throw new Error('Table "product" is empty!');
    }
}

export async function getProductById(id: any) {
    try {
        const { dataValues } = await db.product.findOne({ where: { id } });
        return dataValues;
    } catch (err) {
        throw new Error("This product does not exist!");
    }
}

export async function createProduct(product: object) {
    try {
        const { dataValues } = await db.product.create({ ...product });
        return dataValues;
    } catch (err) {
        throw new Error("Product not created");
    }
}

export async function updateProduct(id:string, product:object) {
    try {
        const { dataValues } = await db.product.update({ ...product }, { where: { id } });
        return dataValues;
    } catch (err) {
        throw new Error("Product id not updated");
    }
}

export async function deleteProduct(id:string) {
    try {
        const { dataValues } = await db.product.destroy({ where: { id } });
        return dataValues;
    } catch (err) {
        throw new Error("Product was not deleted");
    }
}

export async function deleteProducts()  {
    try {
        const { dataValues }  = await db.product.destroy({where: {}});
        return dataValues;
    } catch (err) {
        throw new Error("Products were not deleted");
    }
}