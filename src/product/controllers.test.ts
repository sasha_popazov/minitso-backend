import {
    handleProductCreate,
    handleProductDelete,
    handleProductGet,
    handleProductsDelete,
    handleProductsGet,
    handleProductUpdate,
} from "./controllers";

jest.mock("./services", () => ({
    getProducts: jest.fn().mockReturnValue([
        { id: "1", name: "Four Cheese", price: 5.8 },
        { id: "2", name: "Bavarian pizza", price: 5.4 },
        { id: "3", name: "Seafood pizza", price: 6.3 },
    ]),
    getProductById: jest.fn().mockReturnValue({id: "1", name: "Four Cheese", price: 5.8 }),
    createProduct: jest.fn().mockReturnValue({id: "4", name: "Hunting pizza", price: 5.7}),
    updateProduct: jest.fn().mockReturnValue({id: "3", name: "Seafood pizza", price: 6.3}),
    deleteProduct: jest.fn().mockReturnValue({id: "3", name: "Seafood pizza", price: 6.3}),
    deleteProducts: jest.fn().mockReturnValue({})
}));

const mockResponse = () => {
    return {
        json: jest.fn(),
        status: jest.fn(),
    };
};

const mockNext = () => {
    return jest.fn();
};

describe("Product controller", () => {
    describe("handleProductsGet", () => {
        test("should call res, next methods", async () => {
            const req = {}
            const res = mockResponse();
            const next = mockNext();

            // @ts-ignore
            await handleProductsGet(req, res, next);

            expect(res.json).toHaveBeenCalledWith([
                { id: "1", name: "Four Cheese", price: 5.8 },
                { id: "2", name: "Bavarian pizza", price: 5.4 },
                { id: "3", name: "Seafood pizza", price: 6.3 },
            ]);
            expect(res.status).toHaveBeenCalledWith(200);
            expect(next).toHaveBeenCalled();
        });
    });

    describe("handleProductGet", () => {
        test("should call res, next methods", async () => {
            const req = {params:{id:"2"}}
            const res = mockResponse();
            const next = mockNext();

            // @ts-ignore
            await handleProductGet(req, res, next);

            expect(res.json).toHaveBeenCalledWith(
                {id: "1", name: "Four Cheese", price: 5.8});
            expect(res.status).toHaveBeenCalledWith(200);
            expect(next).toHaveBeenCalled();
        });
    });

    describe("handleProductCreate", () => {
        test("should call res, next methods", async () => {
            const req = {body:{name: "Hunting pizza", price: 5.7}}
            const res = mockResponse();
            const next = mockNext();

            // @ts-ignore
            await handleProductCreate(req, res, next);

            expect(res.json).toHaveBeenCalledWith({
                name: "Hunting pizza",
                price: 5.7
            });
            expect(res.status).toHaveBeenCalledWith(200);
            expect(next).toHaveBeenCalled();
        });
    });

    describe("handleProductUpdate", () => {
        test("should call res, next methods", async () => {
            const req = {params:{id:"3"}, body:{name: "Seafood pizza", price: 6.3}}
            const res = mockResponse();
            const next = mockNext();


            // @ts-ignore
            await handleProductUpdate(req, res, next);

            expect(res.json).toHaveBeenCalledWith({
               name: "Seafood pizza", price: 6.3
            });
            expect(res.status).toHaveBeenCalledWith(200);
            expect(next).toHaveBeenCalled();
        });
    });

    describe("handleProductDelete", () => {
        test("should call res, next methods", async () => {
            const req = {params:{id:"3"}}
            const res = mockResponse();
            const next = mockNext();


            // @ts-ignore
            await handleProductDelete(req, res, next);

            expect(res.json).toHaveBeenCalledWith(`Product with id 3 has been deleted!`);
            expect(res.status).toHaveBeenCalledWith(200);
            expect(next).toHaveBeenCalled();
        });
    });

    describe("handleProductsDelete", () => {
        test("should call res, next methods", async () => {
            const req = {}
            const res = mockResponse();
            const next = mockNext();


            // @ts-ignore
            await handleProductsDelete(req, res, next);

            expect(res.json).toHaveBeenCalledWith(`All products have been deleted!`);
            expect(res.status).toHaveBeenCalledWith(200);
            expect(next).toHaveBeenCalled();
        });
    });

});

