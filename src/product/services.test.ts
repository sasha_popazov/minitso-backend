import {
    getProducts,
    getProductById,
    createProduct,
    updateProduct,
    deleteProduct,
    deleteProducts,
} from "./services";

jest.mock("../db/models", () => ({
    product: {
        findAll: jest.fn(),
        findOne: jest.fn().mockReturnValue( {}),
        create: jest.fn().mockReturnValue({}),
        update: jest.fn().mockReturnValue({}),
        destroy: jest.fn().mockReturnValue({}),
    },
}));

const db = require("../db/models");

describe("Product service", () => {
    describe("getProducts", () => {
        test("should call findAll method", async () => {
            await getProducts();
            expect(db.product.findAll).toHaveBeenCalled();
            expect(db.product.findAll).toBeCalledTimes(1)
        });
    });

    describe("getProductById", () => {
        test("should call findOne method", async () => {
            const id = "1";
            await getProductById(id);
            expect(db.product.findOne).toHaveBeenCalledWith({ where: { id: "1" } });
        });
    });

    describe("createProduct", () => {
        test("should call create method", async () => {
            const product = {
                id: "4",
                name: "Hunting pizza",
                price:  5.7
            };
            await createProduct(product);
            expect(db.product.create).toHaveBeenCalledWith({
                id: "4",
                name: "Hunting pizza",
                price:  5.7
            });
            expect(db.product.create).not.toBeNull();
        });
    });

    describe("updateProduct", () => {
        test("should call update method", async () => {
            const id = "3"
            const product = {
                name: "Seafood pizza",
                price: 6.3
            };
            await updateProduct(id, {...product});
            expect(db.product.update).toHaveBeenCalledWith( product , { where: { id: "3" } });
        });
    });

    describe("deleteProduct", () => {
        test("should call destroy method", async () => {
            const id = "3";
            await deleteProduct(id);
            expect(db.product.destroy).toHaveBeenCalledWith({ where: { id: "3" } });
        });
    });

    describe("deleteProducts", () => {
        test("should call destroy method", async () => {
            await deleteProducts();
            expect(db.product.destroy).toHaveBeenCalled();
        });
    });
});
