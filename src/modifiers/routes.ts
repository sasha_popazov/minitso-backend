import { Router } from "express";
import {
  handleModifiersGet,
  handleModifierGet,
  handleModifierCreate,
  handleModifierUpdate,
  handleModifierDelete,
  handleModifiersDelete,
} from "./controllers";
import {verifyRole} from "../middlewares/verifyRole";

const modifierRoutes = Router();

modifierRoutes.use(verifyRole(1))

modifierRoutes.get("/", handleModifiersGet);
modifierRoutes.get("/:id", handleModifierGet);
modifierRoutes.post("/",  handleModifierCreate);
modifierRoutes.put("/:id", handleModifierUpdate);
modifierRoutes.delete("/:id", handleModifierDelete);
modifierRoutes.delete("/", handleModifiersDelete);

export default modifierRoutes;
