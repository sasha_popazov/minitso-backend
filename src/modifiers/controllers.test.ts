import {
  handleModifierCreate,
  handleModifierDelete,
  handleModifierGet,
  handleModifiersDelete,
  handleModifiersGet,
  handleModifierUpdate,
} from "./controllers";

jest.mock("./services", () => ({
  getModifiers: jest.fn().mockReturnValue([
    { id: "1", name: "Salami", price: 0.79 },
    { id: "2", name: "Parmesan cheese", price: 0.94 },
    { id: "3", name: "Shrimps", price: 1.62 },
    { id: "4", name: 'Olives', price: 0.72 },
  ]),
  getModifierById: jest.fn().mockReturnValue({ id: "4", name: 'Olives', price: 0.72 }),
  createModifier: jest.fn().mockReturnValue({ id: "3", name: "Shrimps", price: 1.62 }),
  updateModifier: jest.fn().mockReturnValue({ id: "2", name: "Parmesan cheese", price: 0.94 }),
  deleteModifier: jest.fn().mockReturnValue({ id: "1", name: "Salami", price: 0.79 }),
  deleteModifiers: jest.fn().mockReturnValue({}),
}));

const mockResponse = () => {
  return {
    json: jest.fn(),
    status: jest.fn(),
  };
};

const mockNext = () => {
  return jest.fn();
};

describe("Modifier controller", () => {
  describe("handleModifiersGet", () => {
    test("should call res, next methods", async () => {
      const req = {};
      const res = mockResponse();
      const next = mockNext();

      // @ts-ignore
      await handleModifiersGet(req, res, next);

      expect(res.json).toHaveBeenCalledWith([
          { id: "1", name: "Salami", price: 0.79 },
          { id: "2", name: "Parmesan cheese", price: 0.94 },
          { id: "3", name: "Shrimps", price: 1.62 },
          { id: "4", name: 'Olives', price: 0.72 },
      ]);
      expect(res.status).toHaveBeenCalledWith(200);
      expect(next).toHaveBeenCalled();
    });
  });

  describe("handleModifierGet", () => {
    test("should call res, next methods", async () => {
      const req = { params: { id: "4" } };
      const res = mockResponse();
      const next = mockNext();

      // @ts-ignore
      await handleModifierGet(req, res, next);

      expect(res.json).toHaveBeenCalledWith({ id: "4", name: 'Olives', price: 0.72 });
      expect(res.status).toHaveBeenCalledWith(200);
      expect(next).toHaveBeenCalled();
    });
  });

  describe("handleModifierCreate", () => {
    test("should call res, next methods", async () => {
      const req = { body: { name: "Shrimps", price: 1.62} };
      const res = mockResponse();
      const next = mockNext();

      // @ts-ignore
      await handleModifierCreate(req, res, next);

      expect(res.json).toHaveBeenCalledWith({
          name: "Shrimps", price: 1.62
      });
      expect(res.status).toHaveBeenCalledWith(200);
      expect(next).toHaveBeenCalled();
    });
  });

  describe("handleModifierUpdate", () => {
    test("should call res, next methods", async () => {
      const req = { params: { id: "2" }, body: { name: "Parmesans cheese", price: 0.94 } };
      const res = mockResponse();
      const next = mockNext();

      // @ts-ignore
      await handleModifierUpdate(req, res, next);

      expect(res.json).toHaveBeenCalledWith({
          name: "Parmesans cheese", price: 0.94
      });
      expect(res.status).toHaveBeenCalledWith(200);
      expect(next).toHaveBeenCalled();
    });
  });

  describe("handleModifierDelete", () => {
    test("should call res, next methods", async () => {
      const req = { params: { id: "1" } };
      const res = mockResponse();
      const next = mockNext();

      // @ts-ignore
      await handleModifierDelete(req, res, next);

      expect(res.json).toHaveBeenCalledWith(
        `Modifier with id 1 has been deleted!`
      );
      expect(res.status).toHaveBeenCalledWith(200);
      expect(next).toHaveBeenCalled();
    });
  });

  describe("handleModifiersDelete", () => {
    test("should call res, next methods", async () => {
      const req = {};
      const res = mockResponse();
      const next = mockNext();

      // @ts-ignore
      await handleModifiersDelete(req, res, next);

      expect(res.json).toHaveBeenCalledWith(
        `All modifiers have been deleted!`
      );
      expect(res.status).toHaveBeenCalledWith(200);
      expect(next).toHaveBeenCalled();
    });
  });
});
