const db = require("../db/models");

export async function getModifiers() {
    try {
        return await db.modifier.findAll();
    } catch (err) {
        throw new Error('Table "modifier" is empty!');
    }
}

export async function getModifierById(id: string) {
    try {
        const { dataValues } = await db.modifier.findOne({ where: { id } });
        return dataValues;
    } catch (err) {
        throw new Error("This modifier does not exist!");
    }
}

export async function createModifier(modifier: object) {
    try {
        const { dataValues } = await db.modifier.create({ ...modifier });
        return dataValues;
    } catch (err) {
        throw new Error("Product not created");
    }
}

export async function updateModifier(id:string, modifier:object) {
    try {
        const { dataValues } = await db.modifier.update({ ...modifier }, { where: { id } });
        return dataValues;
    } catch (err) {
        throw new Error("Modifier id not updated");
    }
}

export async function deleteModifier(id:string) {
    try {
        const { dataValues } = await db.modifier.destroy({ where: { id } });
        return dataValues;
    } catch (err) {
        throw new Error("Modifier was not deleted");
    }
}

export async function deleteModifiers()  {
    try {
        const { dataValues }  = await db.modifier.destroy({where: {}});
        return dataValues;
    } catch (err) {
        throw new Error("Modifiers were not deleted");
    }
}