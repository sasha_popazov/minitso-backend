import {
    getModifiers,
    getModifierById,
    createModifier,
    updateModifier,
    deleteModifier,
    deleteModifiers,
} from "./services";

jest.mock("../db/models", () => ({
    modifier: {
        findAll: jest.fn(),
        findOne: jest.fn().mockReturnValue( {}),
        create: jest.fn().mockReturnValue({}),
        update: jest.fn().mockReturnValue({}),
        destroy: jest.fn().mockReturnValue({}),
    },
}));

const db = require("../db/models");

describe("Modifier service", () => {
    describe("getModifiers", () => {
        test("should call findAll method", async () => {
            await getModifiers();
            expect(db.modifier.findAll).toHaveBeenCalled();
            expect(db.modifier.findAll).toBeCalledTimes(1)
        });
    });

    describe("getModifierById", () => {
        test("should call findOne method", async () => {
            const id = "4";
            await getModifierById(id);
            expect(db.modifier.findOne).toHaveBeenCalledWith({ where: { id: "4" } });
        });
    });

    describe("createModifier", () => {
        test("should call create method", async () => {
            const modifier = {
                id: "7",
                name: "Crackers",
            };
            await createModifier(modifier);
            expect(db.modifier.create).toHaveBeenCalledWith({
                id: "7",
                name: "Crackers",
            });
            expect(db.modifier.create).not.toBeNull();
        });
    });

    describe("updateModifier", () => {
        test("should call update method", async () => {
            const id = "7"
            const modifier = {
                name: "Delicious сrackers",
            };
            await updateModifier(id, {...modifier});
            expect(db.modifier.update).toHaveBeenCalledWith( modifier , { where: { id: "7" } });
        });
    });

    describe("deleteModifier", () => {
        test("should call destroy method", async () => {
            const id = "1";
            await deleteModifier(id);
            expect(db.modifier.destroy).toHaveBeenCalledWith({ where: { id: "1" } });
        });
    });

    describe("deleteModifiers", () => {
        test("should call destroy method", async () => {
            await deleteModifiers();
            expect(db.modifier.destroy).toHaveBeenCalled();
        });
    });
});
