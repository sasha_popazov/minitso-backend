import {
  getModifiers,
  getModifierById,
  createModifier,
  updateModifier,
  deleteModifier,
  deleteModifiers,
} from "./services";
import express from "express";

export async function handleModifiersGet(
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) {
  try {
    const allModifiers = await getModifiers();
    if (allModifiers.length === 0) {
      res.json("Modifier not found!");
    } else {
      res.json(allModifiers);
    }
    res.status(200);
  } catch (error) {
    res.status(404);
  }
  next();
}

export async function handleModifierGet(
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) {
  try {
    const modifierID = req.params.id;
    const someModifier = await getModifierById(modifierID);
    res.json(someModifier);
    res.status(200);
  } catch (error) {
    res.status(404);
  }
  next();
}

export async function handleModifierCreate(
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) {
  try {
    const newModifiers = req.body;
    await createModifier(newModifiers);
    res.json(newModifiers);
    res.status(200);
  } catch (error) {
    res.status(404);
  }
  next();
}

export async function handleModifierUpdate(
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) {
  try {
    const modifierID = req.params.id;
    const updModifier = req.body;
    await updateModifier(modifierID, { ...updModifier });
    res.json(updModifier);
    res.status(200);
  } catch (error) {
    res.status(404);
  }
  next();
}

export async function handleModifierDelete(
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) {
  try {
    const modifierID = req.params.id;
    await deleteModifier(modifierID);
    res.json(`Modifier with id ${modifierID} has been deleted!`);
    res.status(200);
  } catch (error) {
    res.status(404);
  }
  next();
}

export async function handleModifiersDelete(
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) {
  try {
    await deleteModifiers();
    res.json(`All modifiers have been deleted!`);
    res.status(200);
  } catch (error) {
    res.status(404);
  }
  next();
}
