require("dotenv").config();
import express from "express";
import bodyParser from "body-parser";
import cookieParser from "cookie-parser";
import userRoutes from "./user/routes";
import productRoutes from "./product/routes";
import categoryRoutes from "./category/routes";
import authRoutes from "./middlewares/routes";
import modifierRoutes from "./modifiers/routes";
import orderRoutes from "./order/routes";
import paymentRoutes from "./payment/routes";
import cors from "cors";
import { verifyJWT } from "./middlewares/verify";

const app = express();

app.use(cookieParser());

// parse requests of content-type - application/json, application/x-www-form-urlencoded
app.use(bodyParser.json());
app.use(bodyParser.text());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(cors());

// define a route handler for the default home page
app.get("/", (req, res) => {
  let token = req.cookies["random_token"];
  if (!token) {
    res.send("Please sign in the system!");
  } else {
    res.send("Welcome!");
  }
});

// setting authorization endpoints
app.use("/auth", authRoutes);

// verifying users token
app.use(verifyJWT);

// setting users endpoints
app.use("/users", userRoutes);

// setting products endpoints
app.use("/products", productRoutes);

// setting categories endpoints
app.use("/categories", categoryRoutes);

// setting categories endpoints
app.use("/modifiers", modifierRoutes);

// setting order endpoints
app.use("/order", orderRoutes);

// setting order endpoints
app.use("/payment", paymentRoutes);

// starting server
app.listen(process.env.SERVER_PORT, () => {
  console.log(
      `Server has been started at http://localhost:${process.env.SERVER_PORT}`
  );
});
