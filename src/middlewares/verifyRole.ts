import express from "express";
import jwt from "jsonwebtoken";
const { secret } = require("../config/server");

export function verifyRole(role_id: number) {
  return async function (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
      let token = req.cookies["random_token"];
      const decoded = jwt.verify(token, secret);
      // @ts-ignore
      if (decoded.subject.role_id !== role_id) {
        return res.status(403).json("Access denied!");
      } else {
        next();
      }
  };
}
