import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
const db = require("../db/models");
import express from "express";
const { secret } = require("../config/server");
import {getByEmail } from "../user/services";
import * as EmailValidator from 'email-validator';

const generateJWT = (id: string, email: string, role_id: string) => {
  return jwt.sign({ subject: { id, email, role_id } }, secret, {
    expiresIn: "24h",
  });
};

export async function registerUser(
  req: express.Request,
  res: express.Response
) {
  const { first_name, last_name, email, password, role_id } = req.body;

  if (!EmailValidator.validate(email) || !password) {
    res.status(404).json("Email or password are incorrect! Please enter correct email, " +
        "for example: test@gmail.com.");
  }

  const candidate = await getByEmail(email);
  if (candidate) {
    res.status(404).json("User with this email already exists!");
  }
  const user = await db.user.create({
    first_name,
    last_name,
    email,
    password,
    role_id,
  });

  const access_token = generateJWT(user.id, user.email, user.role_id);

  res.cookie('random_token', access_token, { httpOnly: true });
  res.status(201).json({ ...req.body, access_token });
}

export async function loginUser(
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) {
  const { email, password } = req.body;
  const user = await getByEmail(email);

  if (!user) {
    res.json("User not found!");
  }

  const match = await bcrypt.compare(password, user.password);

  if (!match) {
    res.json("Your password is incorrect!");
  } else {
    const token = ({
      user: {
        ...user,
        access_token: generateJWT(user.id, user.email, user.role_id)
      },
    })
    res.cookie('random_token', token.user.access_token, { httpOnly: true });
    res.status(200).json(token);
  }
  next();
}
