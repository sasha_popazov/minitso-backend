import { Router } from "express";
import {loginUser, registerUser} from "./auth";
import {verifyJWT} from "./verify";

const authRoutes = Router();

authRoutes.post("/register", registerUser);
authRoutes.post("/login", loginUser);
authRoutes.post("/verify", verifyJWT);

export default authRoutes;
