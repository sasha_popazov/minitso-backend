import express from "express";
import jwt from "jsonwebtoken";
const { secret } = require("../config/server");

export function verifyJWT(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
) {
  let token = req.cookies["random_token"];
  if (!token) {
    return res.sendStatus(401);
  } else {
    jwt.verify(token, secret, (err: any) => {
      if (err) {
        return res.status(401).json("JWT verify error!");
      } else {
        next();
      }
    });
  }
}

export function getUserFromCookies(
    req: express.Request,
    res: express.Response,
) {
  try {
    let token = req.cookies["random_token"];
    const decoded = jwt.verify(token, secret);
    // @ts-ignore
    if (decoded.subject.role_id === 1) {
      return res.status(403).json("Hey, Admin! You are doing something wrong!");
    } else {
      return decoded
    }
  } catch (e) {
    return console.error("User is not authorized!")
  }
}
