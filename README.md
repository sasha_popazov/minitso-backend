# Minitso backend

## Start serverExpress + postgresDatabase

```
docker-compose up --build
```

## minitso-backend.postman_collection.json

### This file contains a copy of all backend health check requests.

```
To import this file, you need to go to Postman, click on the import button, 
then upload files and specify the path to this file.
```

